// ==UserScript==
// @name         Talibri - Xmitty's Stat Tracker
// @namespace    StatTracker
// @version      0.31
// @description  Track Statistics in Talibri
// @author       Xmitty
// @match        https://talibri.com/*
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js
// @grant        none
// ==/UserScript==

//Globals (What Is Scope?)
var craftingStarted = false;
var combatStarted = false;

var init = {

	initialize: function() {
		$('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>');
		$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />');
		//$('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.4.3/js/tabulator.min.js"></script>');
		//$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.4.3/css/tabulator.min.css" />');
		$('head').append('<script type="text/javascript">var script = script || {};</script>');

		init.initalizeMenus();
		init.initalizeGraphs();

		crafting.data = crafting.new(); // Setting Up Basic Structure
		combat.data = combat.new(); // Setting Up Basic Structure
		
		storage.load(); // Load Previous Data
		
		crafting.start();
		combat.start();
		window.setInterval(init.initalizeMenus, 500);
		window.setInterval(init.initalizeGraphs, 500);
		window.setInterval(crafting.start, 500);
		window.setInterval(combat.start, 500);
		
		},

	initalizeMenus: function() {

		if($(".trackingDropdown").length>0)
		{
			return;
		}

		var menuHtml = '<li class="trackingDropdown">';
		menuHtml += '<a class="trackingDropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded=false">';
		menuHtml += 'StatTracker';
		menuHtml += '<span class="caret"></span>';
		menuHtml += '</a>';
		menuHtml += '<ul class="dropdown-menu">';
		menuHtml += '<li><a>Show StatTracker: <input type="checkbox" id="trackingCheck"> </a></li>';
		menuHtml += '</ul>';
		menuHtml += '</li>';
		$('.navbar-nav').first().append(menuHtml);

		$("#trackingCheck").on('click', function () { init.showUserInterface(); });

	},

	initalizeGraphs: function() {

		if($("#myCraftingChart").length === 0) {
		
			var Html = "";
			Html += '<div id="trackingDialog" title="Stat Tracking" style="display: none">';
			Html += '<div id="tabs_in_dialog"><ul>';
			Html += '<li><a href="#tabs-1">Leol & XP - Live</a></li><li><a href="#tabs-2">Attributes - Live</a></li>'; // Tab Titles
			Html += '<li><a href="#tabs-3">Component Crafting - Live</a></li></li><li><a href="#tabs-4">Combat Breakdown</a></li>';  // Tab Titles
			Html += '<li><a href="#tabs-5">Crafting Breakdown</a></li></li><li><a href="#tabs-6">Settings / Reset</a></li>';  // Tab Titles
			Html += '<div id="tabs-1" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Leol & XP - Live
			Html += '<div id="wrapper" style="position: relative; width: 50%; height: 100%; float: left">'; // Leol Graph
			Html += '<canvas id="myCombatChart"></canvas>';
			Html += '</div>';
			Html += '<div id="wrapper" style="position: relative; width: 50%; height: 100%; float: left">'; // XP Graph
			Html += '<canvas id="myCombatChart2"></canvas>';
			Html += '</div></div>';
			Html += '<div id="tabs-2" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Attribute - Live
			Html += '<div id="wrapper" style="position: relative; width: 100%; height: 100%; float: left">'; // Attributes Graph
			Html += '<canvas id="myCombatChart3"></canvas>';
			Html += '</div></div>';
			Html += '<div id="tabs-3" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Component Crafting - Live
			Html += '<div id="wrapper" style="position: relative; width: 100%; height: 100%; float: left">'; // Component Graph
			Html += '<canvas id="myCraftingChart"></canvas>';
			Html += '</div></div>';
			Html += '<div id="tabs-4" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Combat Breakdown
			Html += '<div id="wrapper" style="position: relative; width: 50%; height: 50%; float: left">'; // Combat Stat Table
			Html += '<table id="combatStatTable" style="width: 80%; margin-left: auto; margin-right: auto; font-size: 20px">';
			Html += '<tr><th>Total Actions</th><td id="combat_actions">0</td></tr>';
			Html += '<tr><th>Attack Accuracy</th><td id="combat_accuracy">0/0</td></tr>';
			Html += '<tr><th>Total Affinity XP Gained</th><td id="combat_exp">0</td></tr>';
			Html += '<tr><th>Total Leol Gained</th><td id="combat_leol">0</td></tr>';
			Html += '<tr><th>Affinity XP/Hour</th><td id="combat_exp_hour">0.00</td></tr>';
			Html += '<tr><th>Leol/Hour</th><td id="combat_leol_hour">0.00</td></tr>';
			Html += '<tr><th></th><td></td></tr>';
			Html += '<tr><th>*** Combat Tick Information ***</th><td id="combat_tick_title"></td></tr>';
			Html += '<tr><th>Average Tick Time</th><td id="combat_tick_time">0.00</td></tr>';
			Html += '<tr><th>Ticks in 24 Hours</th><td id="combat_tick_day">0.00</td></tr>';
			Html += '</table>';
			Html += '</div>';
			Html += '<div id="wrapper" style="position: relative; width: 50%; height: 50%; float: left">'; // Monster Kill Graph
			Html += '<canvas id="myCombatChart4"></canvas>';
			Html += '</div>';
			Html += '<div id="wrapper" style="position: relative; width: 50%; height: 50%; float: left">'; // Combat Stat Table #2
			Html += '<table id="combatStatTable" style="width: 80%; margin-left: auto; margin-right: auto; font-size: 20px">';
			Html += '<tr><th>Total Strength</th><td id="combat_total_str">0</td></tr>';
			Html += '<tr><th>Total Stamina</th><td id="combat_total_stam">0</td></tr>';
			Html += '<tr><th>Total Dexterity</th><td id="combat_total_dex">0</td></tr>';
			Html += '<tr><th>Total Intellect</th><td id="combat_total_int">0</td></tr>';
			Html += '<tr><th>Total Will</th><td id="combat_total_will">0</td></tr>';
			Html += '<tr><th>Attribute XP/Hour</th><td id="combat_att_per_hour">0.00</td></tr>';
			Html += '</table>';
			Html += '</div>';
			Html += '<div id="wrapper" style="position: relative; width: 50%; height: 50%; float: left">'; // Monster Item Graph
			Html += '<canvas id="myCombatChart5"></canvas>';
			Html += '</div></div>';
			Html += '<div id="tabs-5" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Crafting Breakdown
			Html += '<div id="wrapper" style="position: relative; width: 100%; height: 100%; float: left">';
			Html += '<div id="example-table"></div>'; // Testing out Tablulator
			Html += '</div></div>';
			Html += '<div id="tabs-6" style="position: absolute; height: 90%; width: 97%; top: 50px">'; // Settings / Reset
			Html += '<div id="wrapper" style="position: relative; width: 100%; height: 100%; float: left">'; // Reset Buttons
			Html += '<button type="button" id="combatResetBtn">Reset Combat Data</button>';
			Html += '<button type="button" id="craftingResetBtn">Reset Crafting Data</button>';
			Html += '</div></div>';
			Html += '</div></div>';
			
			$('body').append(Html);
			
			$("#combatResetBtn").on('click', function () { combat.reset(); });
			$("#craftingResetBtn").on('click', function () { crafting.reset(); });
		
		
			var craftingConfig = {
				type: 'line',
				data: {
				labels: [0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],
				datasets: [{
					label: "Star Curve",
					data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
					backgroundColor: ['rgba(255, 99, 132, 0.5)'],
					borderColor: ['rgba(255,99,132,1)'],
					fill: false,
					}, ]
				},
				options: {
					maintainAspectRatio: false,
					responsive: true,
					layout: {
						padding: {
							top: 10 }
					},
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Number Of Stars'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Number Of Crafts'
							}
						}]
					}
				}
			};
			
			var randColors = [];
			for (i = 0; i < 10; i++)
			{
				randColors[i] = helper.getRandomColor();
			}
			
			var combatConfig = {
				type: 'line',
				data: {
				labels: [],
				datasets: [{
					label: "Leol/Tick Avg.",
					data: [],
					backgroundColor: randColors[0],
					borderColor: randColors[0],
					yAxisID: "y-axis-1",
					fill: false,
					}, {
					label: "Leol/Sec Avg.",
					data: [],
					backgroundColor: randColors[1],
					borderColor: randColors[1],
					yAxisID: "y-axis-2",
					fill: false,
					}, ]
				},
				options: {
					maintainAspectRatio: false,
					responsive: true,
					title: {
						display:true,
						text:'Leol Per Tick & Second Averages'
					},
					elements: {
						point: {
							radius: 0
						}
					},
					layout: {
						padding: {
							top: 10
						}
					},
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							display: true,
							type: "time",
							time: {
								displayFormats: {
									minute: 'h:mm:ss A',
									hour: 'h:mm A',
									day: 'MMMM D YYYY'
									},
							tooltipFormat: 'MMM D HH:mm:ss'
							},
							scaleLabel: {
								display: true,
								labelString: 'Date'
							}
						}],
						yAxes: [{
							type: "linear",
							display: true,
							position: "left",
							id: "y-axis-1",
							ticks: {
								min: 0
							},
							scaleLabel: {
								display: true,
								labelString: 'Leol'
								}
							}, {
							type: "linear",
							display: true,
							position: "right",
							id: "y-axis-2",
							scaleLabel: {
								display:  true,
								lableString: 'Leol/Tick'
							},
							gridLines: {
								drawOnChartArea: false
							}
						}]
					}
				}
			};

			var combatConfig2 = {
				type: 'line',
				data: {
				labels: [],
				datasets: [{
					label: "Xp/Tick Avg.",
					data: [],
					backgroundColor: randColors[3],
					borderColor: randColors[3],
					yAxisID: "y-axis-1",
					fill: false,
					},{
					label: "Xp/Sec Avg.",
					data: [],
					backgroundColor: randColors[4],
					borderColor: randColors[4],
					yAxisID: "y-axis-2",
					fill: false,
					}, ]
				},
				options: {
					maintainAspectRatio: false,
					responsive: true,
					title: {
						display:true,
						text:'XP Per Tick & Second Averages'
					},
					elements: {
						point: {
							radius: 0
						}
					},
					layout: {
						padding: {
							top: 10
						}
					},
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							display: true,
							type: "time",
							time: {
								displayFormats: {
									minute: 'h:mm:ss A',
									hour: 'h:mm A',
									day: 'MMMM D YYYY'
									},
							tooltipFormat: 'MMM D HH:mm:ss'
							}
						}],
						yAxes: [{
							type: "linear",
							display: true,
							position: "left",
							id: "y-axis-1",
							ticks: {
								min: 0
							},
							scaleLabel: {
								display: true,
								labelString: 'Affinity Experience'
								}
							}, {
							type: "linear",
							display: true,
							position: "right",
							id: "y-axis-2",
							scaleLabel: {
								display:  true,
								lableString: 'Xp/Tick'
							},
							gridLines: {
								drawOnChartArea: false
							}
						}]
					}
				}
			};

			var combatConfig3 = {
				type: 'line',
				data: {
				labels: [],
				},
				options: {
					maintainAspectRatio: false,
					responsive: true,
					title: {
						display:true,
						text:'Attributes Per Tick'
					},
					elements: {
						point: {
							radius: 0
						}
					},
					layout: {
						padding: {
							top: 10
						}
					},
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							display: true,
							type: "time",
							time: {
								displayFormats: {
									minute: 'h:mm:ss A',
									hour: 'h:mm A',
									day: 'MMMM D YYYY'
									},
							tooltipFormat: 'MMM D HH:mm:ss'
							}
						}],
						yAxes: [{
							display: true,
							ticks: {
								min: 0
							},
							scaleLabel: {
								display: true,
								labelString: 'Attributes Per Tick'
							}
						}]
					}
				}
			};

			var combatConfig4 = {
				type: 'pie',
					data: {
						datasets: [{
							data: [0, 0, 0]
						}],
						  labels: [
								'Monster'
							]
					},
				options: {
					maintainAspectRatio: false,
					responsive: true,
					title: {
						display:true,
						text:'Ratio of Enemies Killed'
					},
					layout: {
						padding: {
							top: 10
						}
					},
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
				}
			};
			
			var combatConfig5 = {
				type: 'pie',
					data: {
						datasets: [{
							data: [0, 0, 0]
						}],
						  labels: [
								'Drops'
							]
					},
				options: {
					maintainAspectRatio: false,
					responsive: true,
					title: {
						display:true,
						text:'Ratio of Enemy Drops'
					},
					layout: {
						padding: {
							top: 10
						}
					},
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
				}
			};
		
		var craftingctx = document.getElementById("myCraftingChart").getContext("2d");
		myCraftingLine = new Chart(craftingctx, craftingConfig);
		var combatctx = document.getElementById("myCombatChart").getContext("2d");
		myCombatLine = new Chart(combatctx, combatConfig);
		var combatctx2 = document.getElementById("myCombatChart2").getContext("2d");
		myCombatLine2 = new Chart(combatctx2, combatConfig2);
		var combatctx3 = document.getElementById("myCombatChart3").getContext("2d");
		myCombatLine3 = new Chart(combatctx3, combatConfig3);
		var combatctx4 = document.getElementById("myCombatChart4").getContext("2d");
		myCombatPie1 = new Chart(combatctx4, combatConfig4);
		var combatctx5 = document.getElementById("myCombatChart5").getContext("2d");
		myCombatPie2 = new Chart(combatctx5, combatConfig5);
		
		}
	},
	
	loadTables: function() {
		/*$("#example-table").tabulator({
			height:'auto', // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
			layout:"fitColumns",
			columns:[ //Define Table Columns
				{title:"Name", field:"name", width:150},
				{title:"Age", field:"age", align:"left", formatter:"progress"},
				{title:"Favourite Color", field:"col"},
				{title:"Date Of Birth", field:"dob", sorter:"date", align:"center"},
			],
			rowClick:function(e, row){ //trigger an alert message when the row is clicked
				alert("Row " + row.getData().id + " Clicked!!!!");
			},
		});
		
		var tabledata = [
			{id:1, name:"Oli Bob", age:"12", col:"red", dob:""},
			{id:2, name:"Mary May", age:"1", col:"blue", dob:"14/05/1982"},
			{id:3, name:"Christine Lobowski", age:"42", col:"green", dob:"22/05/1982"},
			{id:4, name:"Brendon Philips", age:"125", col:"orange", dob:"01/08/1980"},
			{id:5, name:"Margret Marmajuke", age:"16", col:"yellow", dob:"31/01/1999"},
		];

		//load sample data into the table
		$("#example-table").tabulator("setData", tabledata);*/
	},
	
	showUserInterface: function () {
		var checkBox = document.getElementById('trackingCheck');
		if (checkBox.checked === true){
			
			//init.loadTables();
			
			$("#trackingDialog").tabs().dialog({
				width: 1300, 
				height: 800,
				modal: true,
				open: function (){
					//$(this).parent().children('.ui-dialog-titlebar').remove();
					//combat.myResize();
				},
				resizeStop: combat.myResize
				});
				$(".trackingDropdown-toggle").dropdown("toggle");
				
				checkBox.checked = false;
				
				//$("#example-table").tabulator("redraw")
				
				
		} else {
			$("#trackingDialog").dialog('close');
		}
	}
};

var storage = {
	STORAGE_KEY: 'TrackingData',
	Structure: {Data: {CraftingData: {StatData: {}, ChartData: {}}, CombatData: {StatData: {}, ChartData: {}}}},
	
	save: function() {
		localStorage.setItem(storage.STORAGE_KEY, JSON.stringify(storage.Structure));
	},
	
	load: function() {
		
		var storedCrafting = localStorage.getItem(storage.STORAGE_KEY);
		
		if (storedCrafting !== undefined) {
			
			var loadedData = {};
			
			storedCrafting = JSON.parse(storedCrafting);
			Object.keys(storedCrafting).forEach(function(key) {
				loadedData[key] = storedCrafting[key];
			});
			
			console.log("We have pulled the key.");
			
			// Handles Loading Crafting Data
			crafting.data = loadedData.Data.CraftingData.ChartData;
			
			for(i = 0; i < 20; i++) {
				myCraftingLine.data.datasets[0].data[i] = crafting.data[i+1];
			}
			
			storage.Structure.Data.CraftingData.ChartData = crafting.data;
			
			console.log("Crafting Data Loaded");
			
			// Handles Loading Combat Data
			combat.data = loadedData.Data.CombatData.ChartData;
			
			
			for(var stat in combat.data.stats) {
				if(combat.data.stats[stat] > -1 && combat.data.statsEnabled[stat] == true) {
					combat.data.statsEnabled[stat] = false;
				}	
			}
			
			if(Object.keys(combat.data.monsters).length > 0) {
				var monList = [];
				var monData = [];
				var monColor = [];
			
				for(var monster in combat.data.monsters) {
					monData.push(combat.data.monsters[monster]);
					monList.push(helper.toTitleCase(monster));
					monColor.push(helper.getRandomColor());		
				}
				
				for(i = 0; i < Object.keys(combat.data.monsters).length; i++)
				{
					combat.monPieKillList.push(monList[i]);
					combat.monPieKillChart.backgroundColor.push(monColor[i]);
					combat.monPieKillChart.borderColor.push(monColor[i]);
					if(i == 0) { myCombatPie1.config.data.labels[i] = (combat.monPieKillList[i]); }
					else { myCombatPie1.config.data.labels.push(combat.monPieKillList[i]); }
				}
	
				combat.monPieKillChart.data = monData;
				combat.totalMonKilled = Object.keys(combat.data.monsters).length;
			}
			
			if(Object.keys(combat.data.loot).length > 0) {
				var monDropList = [];
				var monDropData = [];
				var monDropColor = [];
				var monDropCount = 0;
			
				for(var loots in combat.data.loot) {
					monDropData.push(combat.data.monsters[loots]);
					monDropList.push(helper.toTitleCase(loots));
					monDropColor.push(helper.getRandomColor());		
				}
				
				for(i = 0; i < Object.keys(combat.data.loot).length; i++)
				{
					combat.monPieDropList.push(monDropList[i]);
					combat.monPieDropChart.backgroundColor.push(monDropColor[i]);
					combat.monPieDropChart.borderColor.push(monDropColor[i]);
					if(i == 0) { myCombatPie2.config.data.labels[i] = (combat.monPieDropList[i]); }
					else { myCombatPie2.config.data.labels.push(combat.monPieDropList[i]); }
				}
				
				combat.monPieDropChart.data = monDropData;
				combat.totalMonDrops = Object.keys(combat.data.loot).length;
			}
			
			console.log("Combat Data Loaded");
			
			storage.Structure.Data.CombatData.ChartData = combat.data;
			
			storage.save();
			
			console.log(storage.Structure);
			
		} else {
			storage.save();
		}
	}
};

var crafting = {
	data: null,
	stop: function() {
		craftingStarted = false;
	},
	start: function() {
		var splitURL = document.URL.split("/");
		var findWord = splitURL.indexOf("crafting");

		if(findWord > 0 && craftingStarted == false)
		{
			$(document).ajaxComplete(crafting.listen);
			craftingStarted = true;
		}
		else if(findWord > 0 && craftingStarted == true)
		{
		}
		else if(findWord < 0 && craftingStarted == false)
		{
			craftingStarted = false;
		}
		else
		{
			craftingStarted = false;
		}
	},
	destory: function() {
		crafting.stop();
		$.removeCookie(crafting.cookieName(), {path: crafting.cookiePath()});
	},
	reset: function() {
		console.log("Crafting Resetting.");
		crafting.data = crafting.new();
		storage.Structure.Data.CraftingData.ChartData = crafting.data;
		storage.save();
		myCraftingLine.data.datasets[0].data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		myCraftingLine.update();
	},
	new: function() {
		var empty_data = {
			1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 18: 0, 19: 0, 20: 0,
			total: 0,
			failure: 0,
		};
		return empty_data;
	},
	listen: function(event, xhr, settings) {
		if(craftingStarted == false) {
			$(e.currentTarget).unbind('ajaxComplete');
			return;
		}

		var result = crafting.process(xhr.responseText);
		crafting.logTickResult(result);
	},
	process: function(response) {
		var result = {
			stars: 0,
		};

		var craftingWindow = response.split("\n")[0];
		craftingWindow = craftingWindow.split("html(")[1];
		craftingWindow = craftingWindow.substr(1, craftingWindow.length-4);
		craftingWindow = craftingWindow.replace(/\\n/g,"");
		craftingWindow = craftingWindow.replace(/\\/g,"");
		var craftResult = $(craftingWindow);
		result.stars = ($('.fa-star', craftResult).length * 2 + $('.fa-star-half-o', craftResult).length);

		return result;
	},
	logTickResult: function(result) {
		if(result.stars > 0)
		{
			if(!crafting.data[result.stars]) {
				crafting.data[result.stars] = 1;
				myCraftingLine.data.datasets[0].data[result.stars-1] = crafting.data[result.stars];
			}
			else {
				crafting.data[result.stars] += 1;
				myCraftingLine.data.datasets[0].data[result.stars-1] = crafting.data[result.stars];
			}
		}
		else if(result.stars == 0)
		{
			crafting.data.failure += 1;
		}
		crafting.data.total += 1;
		crafting.data.success = crafting.data.total - crafting.data.failure;
		myCraftingLine.update();
		
		storage.Structure.Data.CraftingData.ChartData = crafting.data;
		
		storage.save();
	}
};

var combat = {
	
	data: null,
	tickCounter: 0,
	totalMonKilled: 0,
	totalMonDrops: 0,
	monPieDropChart: { backgroundColor: [], borderColor: [], data: [], },
	monPieKillChart: { backgroundColor: [], borderColor: [], data: [], },
	monPieKillList: [],
	monPieDropList: [],
	actionDictionary: { skills: [
		{name: "Stab",successful: true, text: "You lunged at the enemy stabbing them"},
		{name: "Stab",successful: false, text: "You attempted to use Stab"},
		{name: "Shock Strike",successful: true, text: "You channel lightning energy into your blade"},
		{name: "Shock Strike",successful: false, text: "You attempted to use Shock Strike"},
		{name: "Lightning Defense",successful: true, text: "Lightning courses through your body"},
		{name: "Lightning Defense",successful: false, text: "You attempted to use Lightning Defense"},
		{name: "Dash",successful: true, text: "You dash into the enemy's defenses"},
		{name: "Bash",successful: true, text: "You bash the enemy dealing"},
		{name: "Bash",successful: false, text: "You attempted to use Bash"},
		{name: "ShieldBash",successful: true, text: "You stunned the enemy!"},
		{name: "Shout",successful: true, text: "You shout at the enemy building your adrenaline"},
		{name: "Fiery Strike",successful: true, text: "You cover your weapon in oil and light it ablaze before striking the enemy"},
		{name: "Fiery Strike",successful: false, text: "You attempted to use Fiery Strike"},
		{name: "Aimed Shot",successful: true, text: "You line up the shot and let your arrow fly"},
		{name: "Aimed Shot",successful: false, text: "You attempted to use Aimed Shot"},
		{name: "Rapid Shot",successful: true, text: "One of your arrows launched in quick succession"},
		{name: "Rapid Shot",successful: false, text: "You attempted to use Rapid Shot"},
		{name: "Wing Clip",successful: true, text: "You aim for the enemy's weapon"},
		{name: "Wing Clip",successful: false, text: "You attempted to use Wing Clip"},
		{name: "Ignite",successful: true, text: "You set your enemy ablaze"},
		{name: "Ignite",successful: false, text: "You attempted to use Ignite"},
		{name: "Freeze",successful: true, text: "You freeze your enemy"},
		{name: "Freeze",successful: false, text: "You attempted to use Freeze"},
		{name: "Electrify",successful: true, text: "You Electrify your enemy"},
		{name: "Electrify",successful: false, text: "You attempted to use Electrify"},
		{name: "Earth Eruption",successful: true, text: "The Earth Erupts under your enemy"},
		{name: "Earth Eruption",successful: false, text: "You attempted to use Earth Eruption"},
		{name: "Item Failed",successful: false, text: "You are out of"},
	]},
	stop: function() {
		combatStarted = false;
	},
	start: function() {
		var splitURL = document.URL.split("/");
		var findWord = splitURL.indexOf("combat_zones");

		if(findWord > 0 && combatStarted == false)
		{
			$(document).ajaxComplete(combat.listen);
			combatStarted = true;
		}
		else if(findWord > 0 && combatStarted == true)
		{
		}
		else if(findWord < 0 && combatStarted == false)
		{
			combatStarted = false;
		}
		else
		{
			combatStarteds = false;
		}
	},
	myResize: function(event, ui) {
		$(this).height($(this).parent().height() - $(this).prev('.ui-dialog-titlebar').height() - 34);
		$(this).width($(this).prev('.ui-dialog-titlebar').width() + 2);
	},
	destory: function() {
		combat.stop();
		$.removeCookie(combat.cookieName(), {path: combat.cookiePath()});
	},
	reset: function() {
		combat.data = combat.new();
		storage.Structure.Data.CombatData.ChartData = combat.data;
		storage.save();
		
	},
	new: function() {
		var empty_data = {
			update: moment().valueOf(),
			since: moment().valueOf(),
			ticks: 0,
			rounds: 0,
			win: 0,
			loss: 0,
			flee: 0,
			inCombat: false,
			leol: 0,
			actions: {},
			items: {},
			monsters: {},
			stats: {strength: 0,stamina: 0, dexterity: 0,will: 0,intellect: 0},
			statsEnabled: {dexterity: false, intellect: false, stamina: false, strength: false, will: false},
			affinities: {},
			loot: {}
		};
		return empty_data;
	},
	listen: function(event, xhr, settings) {
		if(combatStarted == false) {
			$(e.currentTarget).unbind('ajaxComplete');
			return;
		}
		if(settings.url.indexOf('adventure/continue') > -1) {
			if(script.reset_component == true) {
				combat.reset();
			}
			if(!combat.data) {
				combat.data = combat.load();
				if(!combat.data) {
					return;
				}
			}

			var result = combat.getResult(xhr.responseText);

			combat.logBattleRound(result);
		}
	},
	getResult: function(response) {
		var result = {
			player: {
				slain: false
			},
			monster: {
				name: "Unknown",
				slain: false
			},
			leol: 0,
			action: {
				type: "Nothing",
				name: "Unknown",
				successful: false
			},
			stats: {},
			affinities: {},
			loot: {}
		};

		if(response.indexOf('You limped back to town on the verge of death') > -1) {
			result.player.slain = true;
		}

		responseLines = response.split(";");

		for(var i = 0; i < responseLines.length; i++) {
			var item = responseLines[i].toLowerCase();
			if(item.indexOf("$('button:contains(\"") > -1) {
				var userItem = item.substr(item.indexOf("$('button:contains(\"")+20);
				result.action.type = "Item";
				result.action.name = userItem.substr(0,userItem.indexOf(" ("));
				result.action.successful = true;

			} else if(item.indexOf("$combat_round.append") > -1 &&
				item.indexOf("you") > -1)
			{
				var combatText = item.split("\"")[1];

				if(combatText.indexOf("experience.") > -1) {
					var parts = combatText.split(" ");
					result.affinities[parts[3]] = parseInt(parts[2]);
				} else if(combatText.indexOf("leol.") > -1) {
					var parts = combatText.split(" ");
					result.leol = parseInt(parts[2]);
				} else if(combatText.indexOf("experience,") > -1) {
					var parts = combatText
									.replace("you gained ","")
									.split(", ");
					for(var j = 0; j < parts.length; j++){
						var experienceString = parts[j].split(" ");
						result.stats[experienceString[1]] = parseInt(experienceString[0]);
					}
					result.leol = parseInt(parts[2]);
				} else if(combatText.indexOf("you killed the ") > -1) {
					result.monster.name = combatText.replace("you killed the ","").replace("! <br/>","");
					result.monster.slain = true;
				} else if(combatText.indexOf(". you now have ") > -1) {
					var itemStringParts = combatText.substr(11, combatText.indexOf("(")-11).split(" ");
					var itemString = "";
					for(var k = 1; k < itemStringParts.length; k++){
						itemString += (itemString == "" ? "" : " ") + itemStringParts[k];
					}
					result.loot[itemString] = parseInt(itemStringParts[0]);
				} else {
					if(result.action.name == "Unknown") {
						for(var l = 0; l < combat.actionDictionary.skills.length; l++) {
							if(combatText.startsWith(combat.actionDictionary.skills[l].text.toLowerCase())) {
								result.action = combat.actionDictionary.skills[l];
								result.action.type = "Skill";
							}
						}
					}
				}

			}
		}
		return result;
	},
	logBattleRound: function(result){


	
		combat.data.update = moment().valueOf();
		combat.data.rounds += 1;
		combat.data.leol += result.leol;


		if(result.player.slain) {
			combat.data.loss += 1;
		}

		//create action stub if not exists:
		if(result.action.type=="Skill") {
			if(!combat.data.actions[result.action.name]) {
				combat.data.actions[result.action.name] = {successful: 0, unsuccessful: 0};
			}

			//count action:
			if(result.action.successful) {
				combat.data.actions[result.action.name].successful += 1;
			} else {
				combat.data.actions[result.action.name].unsuccessful += 1;
			}
		} else if(result.action.type=="Item") {
			if(!combat.data.items[result.action.name]) {
				combat.data.items[result.action.name] = {count: 1};
			} else {
				combat.data.items[result.action.name].count += 1;
			}
		}
		
		//if monster was slain, count:
		if(result.monster.slain) {
			if(!combat.data.monsters[result.monster.name]) {
				combat.data.monsters[result.monster.name] = 1;
			} else {
				combat.data.monsters[result.monster.name] += 1;
			}
			combat.data.inCombat = false;
			combat.data.win += 1;
		} else {
			combat.data.inCombat = true;
		}

		for(var stat in result.stats)
		{
			if(!combat.data.stats[stat]) {
				combat.data.stats[stat] = result.stats[stat];
			} else {
				combat.data.stats[stat] += result.stats[stat];
			}
		}
		
		for(var affinity in result.affinities)
		{
			if(!combat.data.affinities[affinity]) {
				combat.data.affinities[affinity] = result.affinities[affinity];
			} else {
				combat.data.affinities[affinity] += result.affinities[affinity];
			}
		}

		for(var item in result.loot)
		{
			if(!combat.data.loot[item]) {
				combat.data.loot[item] = result.loot[item];
			} else {
				combat.data.loot[item] += result.loot[item];
			}
		}
		
		storage.Structure.Data.CombatData.ChartData = combat.data;
		
		storage.save();

		combat.updateGraph();
		
		console.log(combat.data.actions);

	},
	updateGraph: function() {

		var now = moment();
		var tickTime = combat.data.update - combat.data.since;
		combat.data.ticks += 1;
		var avgTickTime = tickTime / combat.data.ticks;
		var exp_all = 0;
		for(var affinity in combat.data.affinities) {
			exp_all += combat.data.affinities[affinity];
		}

		//Handles Graph For Leol Data
		if(combat.data.ticks > 0) {
			myCombatLine.data.datasets[0].data.push({
				x: now,
				y: (combat.data.leol / combat.data.ticks).toFixed(2),
			});
			myCombatLine.data.datasets[1].data.push({
				x: now,
				y: (combat.data.leol / ((avgTickTime/1000) * combat.data.ticks)).toFixed(2),
			}); 
		}

		//Handles Graph for Affiny XP
		if(combat.data.ticks > 0) {
			myCombatLine2.data.datasets[0].data.push({
				x: now,
				y: (exp_all / combat.data.ticks).toFixed(2),
			});
			myCombatLine2.data.datasets[1].data.push({
				x: now,
				y: (exp_all / ((avgTickTime/1000) * combat.data.ticks)).toFixed(2),
			}); 
		}

		//Handles Graph for Attribute XP
		var increment = 0;
		for(var stat in combat.data.stats) {
			var newColor = helper.getRandomColor();

			if(combat.data.stats[stat] > -1 && combat.data.statsEnabled[stat] == false) {
				var newDataset = {
					label: stat[0].toUpperCase() + stat.substring(1),
					backgroundColor: newColor,
					borderColor: newColor,
					data: [],
					fill: false
				};
				myCombatLine3.data.datasets.push(newDataset);
				combat.data.statsEnabled[stat] = true;
			}
			myCombatLine3.data.datasets[increment].data.push({
				x: now,
				y: (combat.data.stats[stat] / combat.data.ticks).toFixed(2),
				});
			increment++;
		}

		// Handles Monster Kill Pie Chart
		var monList = [];
		var monData = [];
		var monColor = [];
		var monCount = 0;
		
		for(var monster in combat.data.monsters) {
			monData.push(combat.data.monsters[monster]);
			monList.push(helper.toTitleCase(monster));
			monColor.push(helper.getRandomColor());		
			monCount++;
		}
		
		if(monCount > combat.totalMonKilled) {
			combat.monPieKillList[monCount-1] = monList[monCount-1];
			combat.monPieKillChart.backgroundColor[monCount-1] = monColor[monCount-1];
			combat.monPieKillChart.borderColor[monCount-1] = monColor[monCount-1];
			combat.monPieKillChart.data = monData;
			
			myCombatPie1.config.data.labels = combat.monPieKillList;	
			myCombatPie1.data.datasets[0] = combat.monPieKillChart;
			combat.totalMonKilled = monCount;
		} else {
			combat.monPieKillChart.data = monData;
			myCombatPie1.data.datasets[0] = combat.monPieKillChart;
		}
		
		// Handles Monster Drop Pie Chart
		var monDropList = [];
		var monDropData = [];
		var monDropColor = [];
		var monDropCount = 0;
		
		for(var loots in combat.data.loot) {
			monDropData.push(combat.data.loot[loots]);
			monDropList.push(helper.toTitleCase(loots));
			monDropColor.push(helper.getRandomColor());

			monDropCount++;
		}
		
		if(monDropCount > combat.totalMonDrops) {
			combat.monPieDropList[monDropCount-1] = monDropList[monDropCount-1];
			combat.monPieDropChart.backgroundColor[monDropCount-1] = monDropColor[monDropCount-1];
			combat.monPieDropChart.borderColor[monDropCount-1] = monDropColor[monDropCount-1];
			combat.monPieDropChart.data = monDropData;
			
			myCombatPie2.config.data.labels = combat.monPieDropList;			
			myCombatPie2.data.datasets[0] = combat.monPieDropChart;
			combat.totalMonDrops = monDropCount;

		}
		else
		{
			combat.monPieDropChart.data = monDropData;
			myCombatPie2.data.datasets[0] = combat.monPieDropChart;
		}
		
		// Handles Combat Data Tables
		var runtime = (new Date(combat.data.update) - new Date(combat.data.since))/(3600000);
		var success = 0;
		var failure = 0;
		var totalStat = 0;
		for(var action in combat.data.actions) {
			success += combat.data.actions[action].successful;
			failure += combat.data.actions[action].unsuccessful;
		}
		for(var stat in combat.data.stats) {
			totalStat += combat.data.stats[stat];
		}
		$("#combat_accuracy")[0].innerText = success.toString() + ' of ' + (success+failure).toString() + ' ('+ (Math.round(success/(success+failure)*10000)/100).toString() + '%)';
		$("#combat_actions")[0].innerText = helper.addCommas(combat.data.rounds);
		$("#combat_exp")[0].innerText = helper.addCommas(exp_all);
		$("#combat_leol")[0].innerText = helper.addCommas(combat.data.leol);
		$("#combat_exp_hour")[0].innerText = helper.addCommas(Math.round(exp_all/runtime*100)/100);
		$("#combat_leol_hour")[0].innerText = helper.addCommas(Math.round(combat.data.leol/runtime*100)/100);
		
		$("#combat_tick_time")[0].innerText = helper.addCommas((avgTickTime/1000).toFixed(2)) + ' Seconds';
		$("#combat_tick_day")[0].innerText = helper.addCommas((86400/(avgTickTime/1000)).toFixed(2)) + ' Ticks';
		
		$("#combat_total_str")[0].innerText = helper.addCommas(combat.data.stats.strength);
		$("#combat_total_stam")[0].innerText = helper.addCommas(combat.data.stats.stamina);
		$("#combat_total_dex")[0].innerText = helper.addCommas(combat.data.stats.dexterity);
		$("#combat_total_int")[0].innerText = helper.addCommas(combat.data.stats.intellect);
		$("#combat_total_will")[0].innerText = helper.addCommas(combat.data.stats.will);
		$("#combat_att_per_hour")[0].innerText = helper.addCommas(Math.round(totalStat/runtime*100)/100);
		
		


		if(combat.tickCounter > 120) {
			myCombatLine.data.datasets[0].data.shift();
			myCombatLine.data.datasets[1].data.shift();
			myCombatLine2.data.datasets[0].data.shift();
			myCombatLine2.data.datasets[1].data.shift();
			for(var i = 0; i < increment; i++)
			{
				myCombatLine3.data.datasets[i].data.shift();
			}
		}
		combat.tickCounter++;
		
		myCombatLine.update();
		myCombatLine2.update();
		myCombatLine3.update();
		myCombatPie1.update();
		myCombatPie2.update();

	},
	getString: function() {
		return JSON.stringify(combat.data);
	},
	log: function() {
		console.log(combat.getString());
	}
};

var helper = {
	toTitleCase: function(str)
	{
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	},
	getRandomColor: function() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	},
	addCommas: function(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
};

init.initialize();